<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Address;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $data_user = User::find(Auth::user()->id);
        
        if($data_user->address->user_id == null){
            $array= array(
            "user_id" => $data_user->id,
            "street" => "*Ingrese su direccion",
            "neighborhood" => "*Ingrese el Barrio",
            "city_residence" => "*Ingrese la ciudad",);

            $new_address_user = new Address();
            $new_address_user->create($array);
        }

        $address_user = $data_user->address;

        return view('students.edit', compact('data_user', 'address_user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data_user = User::where('id', $request->id)->first();
        
        $data_user->name = $request->name;
        $data_user->last_name = $request->last_name;
        $data_user->email = $request->email;
        $data_user->phoneNumber = $request->phoneNumber;
        $data_user->hometown = $request->hometown;

        $data_user->save();

        $address_user = $data_user->address;

        return view('students.edit', compact('data_user', 'address_user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
