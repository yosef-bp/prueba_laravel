<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    /**
     * Funcion relacion uno a muchos entre User y Role (inversa)
     */
    public function users(){
        return $this->hasMany('App\Models\User')->withPivot('id');
    }
}
