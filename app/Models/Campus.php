<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campus extends Model
{
    use HasFactory;

    /**
     * Funcion relacion muchos a muchos entre Campus y Faculty
     */
    public function faculties(){
        return $this->belongsToMany('App\Models\Faculty');
    }

    /**
     * Funcion relacion muchos a muchos entre Campus y Program
     */
    public function programs(){
        return $this->belongsToMany('App\Models\Program');
    }
}
