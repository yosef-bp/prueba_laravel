<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    use HasFactory;

    /**
     * Funcion relacion muchos a muchos entre Campus y Faculty
     */
    public function campuses(){
        return $this->belongsToMany('App\Models\Campus');
    }

    /**
     * Funcion relacion uno a muchos entre Faculty y Program
     */
    public function programs(){
        return $this->hasMany('App\Models\Program');
    }
}
