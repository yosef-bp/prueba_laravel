<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'street',
        'neighborhood',
        'city_residence',
    ];

    /**
     * Funcion relacion uno a uno inversa entre Address y User
     */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
