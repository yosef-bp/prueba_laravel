<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    use HasFactory;

    /**
     * Funcion relacion uno a muchos entre Faculty y Program (inversa)
     */
    public function faculty(){
        return $this->belongsTo('App\Models\Faculty')->withPivot('id');
    }

    /**
     * Funcion relacion muchos a muchos entre Campus y Program
     */
    public function campuses(){
        return $this->belongsToMany('App\Models\Campus');
    }
}
