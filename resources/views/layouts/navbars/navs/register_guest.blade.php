<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
  <div class="container">
    <div class="navbar-wrapper">
      <a class="navbar-brand" href="https://mi-prueba-laravel.herokuapp.com/">{{ $title }}</a>
    </div>

        <a style="color: aliceblue;" href="{{ route('login') }}"><i class="material-icons">arrow_back</i>{{ __('Login') }}</a>

  </div>
</nav>
<!-- End Navbar -->