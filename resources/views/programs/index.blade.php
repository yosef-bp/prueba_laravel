@extends('layouts.app')
@section('title', 'Detalle curso')
@section('css')
<link rel="stylesheet" href="/css/style.css">
@stop
@section('content')
@include('partials.navbar_student')
<br>
<div class="table-responsive-md">
<div class=container>
    <table class="table">
            <thead class="table-secondary">
                <tr>
                    <th scope="col">Facultad</th>
                    <th scope="col">Programa</th>
                    <th scope="col">Campus</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($technicianList as $key =>$tecnico)
                    <tr class="table-light">
                        <td>
                            <img src="/img/new_logo.png">                
                        </td>
                        <td>{{ $tecnico['name'] }}</td>
                        <td>{{ $tecnico['neighborhood'] }}</td>
                        <td>{{ $tecnico['ExperienceTime'] }}</td>
                        <td>{{ $tecnico[$typeService] }}</td>
                        <td>
                            <a class="btn btn-outline-primary btn-lg" href="{{ route('list-technician.show', [$tecnico['technician_id'], $typeAppliance, $typeService]) }}" role="button">Ver detalle</a>
                        </td>  
                    </tr>
                    @endforeach
                </tr>
            </tbody>
        </table>
    </div>
</div>
<hr>
@stop

