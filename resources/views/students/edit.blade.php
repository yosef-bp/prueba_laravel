@extends('layouts.app')
@section('title', 'Mi perfil')
@section('css')
<link rel="stylesheet" href="/css/style.css">
@stop
@section('content')
@include('partials.navbar_student')
<br>
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('student.update', $data_user) }}">
        @csrf
        @method('PUT')
        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Actualizar datos personales') }}</strong></h4>
          </div>
          <div class="card-body ">

          <div class="bmd-form-group mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">school</i>
                  </span>
                </div>
                <div class="form-check">                        
                  <input type="radio" name="role_id" id="role_id" value="1" checked>
                </div>
              </div>
            </div>

            <hr>

            <div class="bmd-form-group{{ $errors->has('name') || $errors->has('last_name') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">face</i>
                  </span>
                </div>
                <input type="text" name="name" class="form-control" placeholder="{{ __('Nombre...') }}" value="{{ $data_user->name }}" required>
              </div>
              @if ($errors->has('name'))
                <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                  <strong>{{ $errors->first('name') }}</strong>
                </div>
              @endif

              <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">arrow_forward</i>
                  </span>
                </div>
              <input type="text" name="last_name" class="form-control" placeholder="{{ __('Apellido...') }}" value="{{ $data_user->last_name }}" required>
              </div>
              @if ($errors->has('last_name'))
                <div id="name-error" class="error text-danger pl-3" for="last_name" style="display: block;">
                  <strong>{{ $errors->first('last_name') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" value="{{ $data_user->email }}" required>
              </div>
              @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('phoneNumber') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">add_ic_call</i>
                  </span>
                </div>
                <input type="phoneNumber" name="phoneNumber" id="phoneNumber" class="form-control" placeholder="{{ __('# telefono...') }}" value="{{ $data_user->phoneNumber }}" required>
              </div>
              @if ($errors->has('phoneNumber'))
                <div id="name-error" class="error text-danger pl-3" for="phoneNumber" style="display: block;">
                  <strong>{{ $errors->first('phoneNumber') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('hometown') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">fmd_good</i>
                  </span>
                </div>
                <input type="hometown" name="hometown" id="hometown" class="form-control" placeholder="{{ __('Ciudad de origen...') }}" value="{{ $data_user->hometown }}" required>
              </div>
              @if ($errors->has('hometown'))
                <div id="name-error" class="error text-danger pl-3" for="hometown" style="display: block;">
                  <strong>{{ $errors->first('hometown') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('nationality') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">flag</i>
                  </span>
                </div>
                <input type="nationality" name="nationality" id="nationality" class="form-control" placeholder="{{ __('Nacionalidad...') }}" value="{{ $data_user->nationality }}" required>
              </div>
              @if ($errors->has('nationality'))
                <div id="name-error" class="error text-danger pl-3" for="nationality" style="display: block;">
                  <strong>{{ $errors->first('nationality') }}</strong>
                </div>
              @endif
            </div>

          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary ">{{ __('Actualizar') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<br>

<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form method="POST" action="{{ route('address.update', $address_user) }}">
        @csrf
        @method('PUT')
        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Actualizar direccion de residencia') }}</strong></h4>
          </div>
          <div class="card-body ">

            <div class="bmd-form-group{{ $errors->has('street') || $errors->has('street') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">map</i>
                  </span>
                </div>
                <input type="text" name="street" class="form-control" placeholder="{{ __('Direccion...') }}" value="{{ $address_user->street }}" required>
              </div>
              @if ($errors->has('street'))
                <div id="name-error" class="error text-danger pl-3" for="street" style="display: block;">
                  <strong>{{ $errors->first('street') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('neighborhood') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">apartment</i>
                  </span>
                </div>
                <input type="neighborhood" name="neighborhood" class="form-control" placeholder="{{ __('Barrio...') }}" value="{{ $address_user->neighborhood }}" required>
              </div>
              @if ($errors->has('neighborhood'))
                <div id="email-error" class="error text-danger pl-3" for="neighborhood" style="display: block;">
                  <strong>{{ $errors->first('neighborhood') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('phoneNumber') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">fmd_good</i>
                  </span>
                </div>
                <input type="city_residence" name="city_residence" id="city_residence" class="form-control" placeholder="{{ __('# Ciudad de residencia...') }}" value="{{ $address_user->city_residence }}" required>
              </div>
              @if ($errors->has('city_residence'))
                <div id="name-error" class="error text-danger pl-3" for="city_residence" style="display: block;">
                  <strong>{{ $errors->first('city_residence') }}</strong>
                </div>
              @endif
            </div>

          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary ">{{ __('Actualizar') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@include('partials.footer')
@endsection

