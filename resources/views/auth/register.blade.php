@extends('layouts.register_main', ['class' => 'off-canvas-sidebar', 'activePage' => 'register', 'title' => __('Prueba Laravel')])
@section('title', 'Registro')

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Registrarse') }}</strong></h4>
          </div>
          <div class="card-body ">

          <div class="bmd-form-group mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">school</i>
                  </span>
                </div>
                <div class="form-check">                        
                  <input type="radio" name="role_id" id="role_id" value="1" checked>
                </div>
              </div>
            </div>

            <div class="bmd-form-group mt-8">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">manage_accounts</i>
                  </span>
                </div>
                <div class="form-check">                        
                  <input type="radio" name="role_id" id="role_id" value="2">
                </div>
              </div>
            </div>
            <hr>

            <div class="bmd-form-group{{ $errors->has('name') || $errors->has('last_name') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">face</i>
                  </span>
                </div>
                <input type="text" name="name" class="form-control" placeholder="{{ __('Nombre...') }}" value="{{ old('name') }}" required>
              </div>
              @if ($errors->has('name'))
                <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                  <strong>{{ $errors->first('name') }}</strong>
                </div>
              @endif

              <div class="input-group">
               <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">arrow_forward</i>
                  </span>
                </div>
              <input type="text" name="last_name" class="form-control" placeholder="{{ __('Apellido...') }}" value="{{ old('last_name') }}" required>
              </div>
              @if ($errors->has('last_name'))
                <div id="name-error" class="error text-danger pl-3" for="last_name" style="display: block;">
                  <strong>{{ $errors->first('last_name') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" value="{{ old('email') }}" required>
              </div>
              @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('phoneNumber') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">add_ic_call</i>
                  </span>
                </div>
                <input type="phoneNumber" name="phoneNumber" id="phoneNumber" class="form-control" placeholder="{{ __('# telefono...') }}" required>
              </div>
              @if ($errors->has('phoneNumber'))
                <div id="name-error" class="error text-danger pl-3" for="phoneNumber" style="display: block;">
                  <strong>{{ $errors->first('phoneNumber') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('hometown') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">fmd_good</i>
                  </span>
                </div>
                <input type="hometown" name="hometown" id="hometown" class="form-control" placeholder="{{ __('Ciudad de origen...') }}" required>
              </div>
              @if ($errors->has('hometown'))
                <div id="name-error" class="error text-danger pl-3" for="hometown" style="display: block;">
                  <strong>{{ $errors->first('hometown') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('nationality') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">flag</i>
                  </span>
                </div>
                <input type="nationality" name="nationality" id="nationality" class="form-control" placeholder="{{ __('Nacionalidad...') }}" required>
              </div>
              @if ($errors->has('nationality'))
                <div id="name-error" class="error text-danger pl-3" for="nationality" style="display: block;">
                  <strong>{{ $errors->first('nationality') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password...') }}" required>
              </div>
              @if ($errors->has('password'))
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>{{ $errors->first('password') }}</strong>
                </div>
              @endif
            </div>

            <div class="bmd-form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">password</i>
                  </span>
                </div>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirmar Password...') }}" required>
              </div>
              @if ($errors->has('password_confirmation'))
                <div id="password_confirmation-error" class="error text-danger pl-3" for="password_confirmation" style="display: block;">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                </div>
              @endif
            </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Crear cuenta') }}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
