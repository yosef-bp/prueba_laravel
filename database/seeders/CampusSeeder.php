<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Campus;

class CampusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // crear campus de Cali
        $new_campus = new Campus();

        $new_campus->name_campus = "Cali";
        $new_campus->save();

        // crear campus de Medellin
        $new_campus = new Campus();

        $new_campus->name_campus = "Medellin";
        $new_campus->save();

        // crear campus de Bogota
        $new_campus = new Campus();

        $new_campus->name_campus = "Bogota";
        $new_campus->save();
    }
}
