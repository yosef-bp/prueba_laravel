<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Program;
use App\Models\User;
use App\Models\Address;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(RoleSeeder::class);
        User::factory(100)->create();
        $this->call(CampusSeeder::class);
        $this->call(FacultySeeder::class);
        $this->call(ProgramSeeder::class);
        Address::factory(100)->create();
    }
}
