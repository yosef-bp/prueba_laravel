<?php

namespace Database\Seeders;
use App\Models\Faculty;

use Illuminate\Database\Seeder;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // crear facultad Agronomía
        $new_faculty = new Faculty();

        $new_faculty->name_faculty = "Agronomía";
        $new_faculty->save();

        // crear facultad Medicina
        $new_faculty = new Faculty();

        $new_faculty->name_faculty = "Medicina";
        $new_faculty->save();

        // crear facultad Ingeniería
        $new_faculty = new Faculty();

        $new_faculty->name_faculty = "Ingeniería";
        $new_faculty->save();
    }
}
