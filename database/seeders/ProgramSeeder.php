<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Program;
use App\Models\Faculty;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // crear programa Ingeniería Agrícola
        $new_program = new Program();

        $new_program->name_program = "Ingenieria Agricola";
        $new_program->faculty_id = Faculty::find(1)->id;
        $new_program->save();

        // crear programa Ingeniería Civil
        $new_program = new Program();

        $new_program->name_program = "Ingeniería Civil";
        $new_program->faculty_id = Faculty::find(3)->id;
        $new_program->save();

        // crear programa Ingeniería Software
        $new_program = new Program();

        $new_program->name_program = "Ingeniería Software";
        $new_program->faculty_id = Faculty::find(3)->id;
        $new_program->save();

        // crear programa Enfermería
        $new_program = new Program();

        $new_program->name_program = "Enfermería";
        $new_program->faculty_id = Faculty::find(2)->id;
        $new_program->save();
    }
}
