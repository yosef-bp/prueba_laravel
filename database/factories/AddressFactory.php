<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;


class AddressFactory extends Factory
{
    var $id_search;
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {   
        return [
            'user_id' => User::all()->random()->id,
            'street' => $this->faker->streetAddress,
            'neighborhood' => $this->faker->streetName,
            'city_residence' => $this->faker->randomElement(['Bogota', 'Medellin', 'Cali']),
        ];
    }
}

